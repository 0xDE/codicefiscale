# CodiceFiscale

**CodiceFiscale** is an Android application that allows you to easily calculate and store the so-called "[Codice Fiscale][CF]", a code that uniquely identifies people residing in Italy.

## Notes
Please keep in mind that in some cases more than one person might have been assigned the same *codice fiscale* (*e.g.*, two people with the same name and surname, born the same day in the same city). This problem is called "**omocodia**". In such cases, the Italian authority *Agenzia delle Entrate* disambiguates the two codes by changing one or more digits, starting from the rightmost one. Unluckily, we cannot know *a priori* if a given *codice fiscale* has already been assigned, *i.e.*, if a *omocodia* problem stands.

Given this possibility, the only way to be sure to obtain the **exact** Codice Fiscale is to contact the *Agenzia delle Entrate*.

[CF]: <https://it.wikipedia.org/wiki/Codice_fiscale>
