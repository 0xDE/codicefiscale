package com.gmail.ndev.codicefiscale.controller;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gmail.ndev.codicefiscale.R;
import com.gmail.ndev.codicefiscale.model.CursorAdapter;
import com.gmail.ndev.codicefiscale.model.FiscalCodeCursorAdapter;
import com.gmail.ndev.codicefiscale.model.FiscalCodeDbHelper;
import com.gmail.ndev.codicefiscale.model.FiscalCodeEntry;
import com.gmail.ndev.codicefiscale.view.SimpleDividerItemDecoration;

/**
 * Created by nicola on 22/12/16.
 */

public class ListFiscalCodeFragment extends Fragment implements View.OnClickListener {

    private RecyclerView mRecyclerView;
    private SQLiteDatabase mDatabase;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.list_fiscal_codes, container, false);

        mRecyclerView = ((RecyclerView) view.findViewById(android.R.id.list));
        view.findViewById(R.id.insert_fiscal_code).setOnClickListener(this);

        // This recycler view has always the same size, regardless of the content
        mRecyclerView.setHasFixedSize(false);

        // Add support to swipe-to-delete
        final ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                ((FiscalCodeCursorAdapter) mRecyclerView.getAdapter()).removeItem(viewHolder.getItemId(), viewHolder.getAdapterPosition());
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                switch (actionState) {
                    case ItemTouchHelper.ACTION_STATE_SWIPE:
                        if (dX < 0) {
                            // The trashbin icon
                            Bitmap icon;

                            // The row
                            View itemView = viewHolder.itemView;

                            Paint p = new Paint();
                            // The background will be the app's accent color
                            p.setColor(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark));
                            RectF background = new RectF(itemView.getRight() + dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                            c.drawRect(background, p);
                            icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_black_24dp);

                            /*
                             * We need the icon to fit inside the row. The shortest side is the height.
                             * Since we want the icon to be vertically centered, we need each side to
                             * measure itemView.height/3.
                             */
                            float size = itemView.getHeight() / 3;

                            // We are swiping towards left, hence dX is NEGATIVE!
                            Rect icon_part = new Rect(0, 0, (int) Math.min(icon.getWidth(), Math.abs(dX)), icon.getHeight());

                            /*
                             * When the trashbin is fully shown, we will add some right&left additional padding
                             * in order to make the icon more centered
                             */
                            float trashbinAdditionalPadding = 0F;
                            if (Math.abs(dX) > size) {
                                trashbinAdditionalPadding = Math.min(size, Math.abs(dX) - size);
                            }

                            RectF icon_dest = new RectF(itemView.getRight() - Math.min(size, Math.abs(dX)) - trashbinAdditionalPadding / 2, itemView.getTop() + size, itemView.getRight() - trashbinAdditionalPadding / 2, itemView.getBottom() - size);

                            c.drawBitmap(icon, icon_part, icon_dest, p);
                        }
                        break;
                }

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        });

        itemTouchHelper.attachToRecyclerView(mRecyclerView);

        // Set layout for the RecyclerView
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.insert_fiscal_code:
                Fragment nextFragment = new InsertFiscalCodeFragment();

                getFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.container, nextFragment)
                        .commit();
                break;
        }
    }

    private Cursor queryDatabase() {
        if (mDatabase == null) {
            mDatabase = new FiscalCodeDbHelper(getContext()).getReadableDatabase();
        }

        return mDatabase.query(FiscalCodeEntry.TABLE_NAME, null, null, null, null, null, null);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Get the list of fiscal codes
        Cursor cursor = queryDatabase();

        FiscalCodeCursorAdapter adapter = new FiscalCodeCursorAdapter(getContext(), cursor, new CursorAdapter.Callback() {
            @Override
            public Cursor requery() {
                return queryDatabase();
            }

            @Override
            public void deleteItem(long id) {
                SQLiteDatabase db = new FiscalCodeDbHelper(getContext()).getWritableDatabase();
                db.delete(FiscalCodeEntry.TABLE_NAME, FiscalCodeEntry._ID + " = ?", new String[]{String.valueOf(id)});
                db.close();
            }
        });

        mRecyclerView.setAdapter(adapter);

        // Add a horizontal separator between the list items
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        // Setup a listener for RecyclerView's items
        RecyclerItemClickListener onItemTouchListener = new RecyclerItemClickListener(getContext(), mRecyclerView);
        onItemTouchListener.setListener(new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, long id) {
                Fragment detailFragment = new FiscalCodeDetailFragment();
                Bundle args = new Bundle(1);
                args.putLong(FiscalCodeDetailFragment.EXTRA_ITEM_ID, id);
                detailFragment.setArguments(args);

                // Setup transition names
                ViewCompat.setTransitionName(view.findViewById(R.id.row_fiscal_code), getString(R.string.transition_fiscal_code));
                ViewCompat.setTransitionName(view.findViewById(R.id.row_fullname), getString(R.string.transition_name));
                ViewCompat.setTransitionName(view.findViewById(R.id.row_place_of_birth), getString(R.string.transition_place_of_birth));
                ViewCompat.setTransitionName(view.findViewById(R.id.row_date_of_birth), getString(R.string.transition_date_of_birth));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Transition moveTransition = TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move);
                    detailFragment.setSharedElementEnterTransition(moveTransition);
                    detailFragment.setSharedElementReturnTransition(moveTransition);
                    detailFragment.setEnterTransition(new Fade(Fade.IN));
                    detailFragment.setExitTransition(new Fade(Fade.OUT));

                    setSharedElementEnterTransition(moveTransition);
                    setSharedElementReturnTransition(moveTransition);
                    setEnterTransition(new Fade(Fade.IN));
                    setExitTransition(new Fade(Fade.OUT));
                }
                FragmentTransaction transaction = getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, detailFragment)
                        .addToBackStack(null);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    transaction.addSharedElement(view.findViewById(R.id.row_fiscal_code), getString(R.string.transition_fiscal_code));
                    transaction.addSharedElement(view.findViewById(R.id.row_fullname), getString(R.string.transition_name));
                    transaction.addSharedElement(view.findViewById(R.id.row_place_of_birth), getString(R.string.transition_place_of_birth));
                    transaction.addSharedElement(view.findViewById(R.id.row_date_of_birth), getString(R.string.transition_date_of_birth));
                }

                transaction.commit();
            }
        });

        mRecyclerView.addOnItemTouchListener(onItemTouchListener);
    }

    @Override
    public void onStop() {
        super.onStop();

        // Close the cursor and the database
        ((CursorAdapter) mRecyclerView.getAdapter()).changeCursor(null);
        if (mDatabase != null) {
            mDatabase.close();
            mDatabase = null;
        }
    }
}
