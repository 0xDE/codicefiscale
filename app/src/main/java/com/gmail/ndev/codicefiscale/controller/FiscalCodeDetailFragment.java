package com.gmail.ndev.codicefiscale.controller;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ShareActionProvider;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gmail.ndev.codicefiscale.R;
import com.gmail.ndev.codicefiscale.model.CFGenerator;
import com.gmail.ndev.codicefiscale.model.FiscalCodeDbHelper;
import com.gmail.ndev.codicefiscale.model.FiscalCodeEntry;
import com.gmail.ndev.codicefiscale.model.ICFGenerator;
import com.gmail.ndev.codicefiscale.model.PlaceOfBirthEntry;
import com.gmail.ndev.codicefiscale.model.PlaceOfBirthProvider;

import java.util.Date;

/**
 * Created by nicola on 06/01/17.
 */
public class FiscalCodeDetailFragment extends Fragment implements View.OnClickListener {

    public static final String EXTRA_ITEM_ID = "extra_item_id";

    private TextView mName, mPlaceOfBirth, mDateOfBirth, mFiscalCode;
    private Button mCopyToClipboard, mOmocodeVariants;
    private ImageButton mOmocodeInfoBtn;

    private long mId = -1L;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fiscal_code_detail, container, false);

        mName = ((TextView) view.findViewById(R.id.name));
        mDateOfBirth = ((TextView) view.findViewById(R.id.date_of_birth));
        mPlaceOfBirth = ((TextView) view.findViewById(R.id.place_of_birth));
        mFiscalCode = ((TextView) view.findViewById(R.id.fiscal_code));
        mCopyToClipboard = ((Button) view.findViewById(R.id.btn_copy_to_clipboard));
        mOmocodeVariants = ((Button) view.findViewById(R.id.btn_omocode_variants));
        mOmocodeInfoBtn = ((ImageButton) view.findViewById(R.id.btn_show_omocode_info));

        mCopyToClipboard.setOnClickListener(this);
        mOmocodeVariants.setOnClickListener(this);
        mOmocodeInfoBtn.setOnClickListener(this);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.detail_option_menu, menu);

        MenuItem item = menu.findItem(R.id.menu_share);

        ShareActionProvider shareActionProvider = ((ShareActionProvider) MenuItemCompat.getActionProvider(item));

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, mFiscalCode.getText());
        shareIntent.setType("text/plain");
        shareActionProvider.setShareIntent(shareIntent);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getArguments() == null)
            return;

        mId = getArguments().getLong(EXTRA_ITEM_ID, -1L);

        updateTextFields();
    }

    private void updateTextFields() {
        SQLiteDatabase database = new FiscalCodeDbHelper(getContext()).getReadableDatabase();

        Cursor cursor = database.query(FiscalCodeEntry.TABLE_NAME, null, FiscalCodeEntry._ID + " = ?", new String[]{Long.toString(mId)}, null, null, null);

        if (cursor.moveToFirst()) {
            String fiscalCode = cursor.getString(cursor.getColumnIndex(FiscalCodeEntry.COLUMN_CODE));
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(cursor.getString(cursor.getColumnIndex(FiscalCodeEntry.COLUMN_NAME)));
            stringBuilder.append(' ');
            stringBuilder.append(cursor.getString(cursor.getColumnIndex(FiscalCodeEntry.COLUMN_SURNAME)));

            long placeOfBirthId = cursor.getLong(cursor.getColumnIndex(FiscalCodeEntry.COLUMN_PLACE_OF_BIRTH));
            // Dates are store in seconds, while Java requires milliseconds
            Date dateOfBirth = new Date(1000L*cursor.getLong(cursor.getColumnIndex(FiscalCodeEntry.COLUMN_DATE_OF_BIRTH)));

            Uri uri = new Uri.Builder()
                    .scheme("content")
                    .authority(PlaceOfBirthProvider.AUTHORITY)
                    .appendPath("places")
                    .appendPath(String.valueOf(placeOfBirthId))
                    .build();
            Cursor placeOfBirthCursor = getContext().getContentResolver().query(uri, null, null, null, null);
            mName.setText(stringBuilder.toString());
            if (placeOfBirthCursor.moveToFirst()) {
                String placeOfBirth = placeOfBirthCursor.getString(placeOfBirthCursor.getColumnIndex(PlaceOfBirthEntry.COLUMN_NAME_CITY_NAME));
                mPlaceOfBirth.setText(placeOfBirth);
            }
            placeOfBirthCursor.close();
            mDateOfBirth.setText(DateFormat.getLongDateFormat(getContext()).format(dateOfBirth));
            mFiscalCode.setText(fiscalCode);
        }
        cursor.close();
        database.close();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_copy_to_clipboard:
                ClipboardManager clipboardManager = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clipData = ClipData.newPlainText("Fiscal code", mFiscalCode.getText());

                clipboardManager.setPrimaryClip(clipData);

                Snackbar snackbar = Snackbar.make(getView(), R.string.copied, Snackbar.LENGTH_SHORT);
                snackbar.show();
                break;

            case R.id.btn_omocode_variants:

                ICFGenerator icfGenerator = new CFGenerator();
                final String[] variants = icfGenerator.computeOmocodes(mFiscalCode.getText().toString());
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                        .setTitle(R.string.choose_fiscal_code_variant)
                        .setItems(variants, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int position) {

                                String variant = variants[position];

                                updateFiscalCode(variant);
                                dialogInterface.dismiss();
                            }
                        })
                        .setNegativeButton(R.string.cancel, null);

                builder.show();
                break;

            case R.id.btn_show_omocode_info:
                builder = new AlertDialog.Builder(getContext())
                        .setMessage(R.string.omocode_info)
                        .setPositiveButton(R.string.ok, null);

                builder.show();
                break;
            default:
                return;
        }
    }

    private void updateFiscalCode(String newFiscalCode) {
        if (mId == -1L) {
            return;
        }

        SQLiteDatabase db = new FiscalCodeDbHelper(getContext()).getWritableDatabase();
        ContentValues values = new ContentValues(1);
        values.put(FiscalCodeEntry.COLUMN_CODE, newFiscalCode);
        db.update(FiscalCodeEntry.TABLE_NAME, values, FiscalCodeEntry._ID + " = ?", new String[] { String.valueOf(mId)});
        db.close();

        updateTextFields();
    }
}
