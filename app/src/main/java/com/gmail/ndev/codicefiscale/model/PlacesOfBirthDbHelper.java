package com.gmail.ndev.codicefiscale.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by nicola on 21/12/16.
 */

public class PlacesOfBirthDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "PlacesOfBirth.db";

    private static final String SQL_CREATE_DB = "CREATE TABLE " + PlaceOfBirthEntry.TABLE_NAME + " (" +
            PlaceOfBirthEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            PlaceOfBirthEntry.COLUMN_NAME_CITY_NAME + " TEXT NOT NULL, " +
            PlaceOfBirthEntry.COLUMN_NAME_CITY_CODE + " TEXT NOT NULL)";

    private static final String SQL_DELETE_DB = "DROP TABLE IF EXISTS " + PlaceOfBirthEntry.TABLE_NAME;
    private final Context context;

    public PlacesOfBirthDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public Cursor fetchAll() {
        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor = database.query(PlaceOfBirthEntry.TABLE_NAME, new String[]{PlaceOfBirthEntry._ID, PlaceOfBirthEntry.COLUMN_NAME_CITY_NAME, PlaceOfBirthEntry.COLUMN_NAME_CITY_CODE}, null, null, null, null, null);
        database.close();
        return cursor;
    }

    public Cursor fetchCityLike(String city) {
        SQLiteDatabase database = getReadableDatabase();

        Cursor cursor = database.query(PlaceOfBirthEntry.TABLE_NAME, new String[]{PlaceOfBirthEntry._ID, PlaceOfBirthEntry.COLUMN_NAME_CITY_NAME, PlaceOfBirthEntry.COLUMN_NAME_CITY_CODE}, PlaceOfBirthEntry.COLUMN_NAME_CITY_NAME + " LIKE ?", new String[]{"%"+city+"%"}, null, null, null);
        database.close();
        return cursor;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_DB);

        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(context.getAssets().open("places.sql")));

            String line;
            while ((line = br.readLine()) != null) {
                sqLiteDatabase.execSQL(line);
            }

            br.close();
        } catch (IOException e) {
            Log.e("CodiceFiscale", e.toString());
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ignored) {
                }
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(SQL_DELETE_DB);
        onCreate(sqLiteDatabase);
    }
}
