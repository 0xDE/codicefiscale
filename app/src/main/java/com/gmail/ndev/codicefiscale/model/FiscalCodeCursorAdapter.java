package com.gmail.ndev.codicefiscale.model;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gmail.ndev.codicefiscale.R;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by nicola on 01/01/17.
 */

public class FiscalCodeCursorAdapter extends CursorAdapter {

    public FiscalCodeCursorAdapter(Context context, Cursor cursor, Callback callback) {
        super(context, cursor, callback);
    }

    @Override
    void onBindViewHolder(RecyclerView.ViewHolder holder, Cursor cursor) {
        ViewHolder vh = ((ViewHolder) holder);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(cursor.getString(cursor.getColumnIndex(FiscalCodeEntry.COLUMN_NAME)));
        stringBuilder.append(' ');
        stringBuilder.append(cursor.getString(cursor.getColumnIndex(FiscalCodeEntry.COLUMN_SURNAME)));
        vh.mFullName.setText(stringBuilder.toString());
        vh.mFiscalCode.setText(cursor.getString(cursor.getColumnIndex(FiscalCodeEntry.COLUMN_CODE)));

        long placeOfBirthId = cursor.getLong(cursor.getColumnIndex(FiscalCodeEntry.COLUMN_PLACE_OF_BIRTH));
        Cursor cursorPlaceOfBirth = getContext().getContentResolver().query(Uri.parse("content://"+PlaceOfBirthProvider.AUTHORITY+"/places/"+placeOfBirthId), null, null, null, null);

        if (!cursorPlaceOfBirth.moveToFirst()) {
            Log.e("TAG", "Error: cannot move to first element in cursor");
        } else {
            String placeOfBirth = cursorPlaceOfBirth.getString(cursorPlaceOfBirth.getColumnIndex(PlaceOfBirthEntry.COLUMN_NAME_CITY_NAME));
            vh.mPlaceOfBirth.setText(placeOfBirth);
        }

        cursorPlaceOfBirth.close();
        // Format date of birth
        DateFormat dateFormat = android.text.format.DateFormat.getLongDateFormat(getContext());
        String formattedDate = dateFormat.format(new Date(1000L*cursor.getLong(cursor.getColumnIndex(FiscalCodeEntry.COLUMN_DATE_OF_BIRTH))));
        vh.mDateOfBirth.setText(formattedDate);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.fiscal_code_row, parent, false);
        return new ViewHolder(view);

    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

        TextView mFullName, mPlaceOfBirth, mDateOfBirth, mFiscalCode;

        ViewHolder(View itemView) {
            super(itemView);

            mFullName = ((TextView) itemView.findViewById(R.id.row_fullname));
            mDateOfBirth = ((TextView) itemView.findViewById(R.id.row_date_of_birth));
            mFiscalCode = ((TextView) itemView.findViewById(R.id.row_fiscal_code));
            mPlaceOfBirth = ((TextView) itemView.findViewById(R.id.row_place_of_birth));
        }
    }
}
