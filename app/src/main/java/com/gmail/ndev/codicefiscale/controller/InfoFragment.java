package com.gmail.ndev.codicefiscale.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gmail.ndev.codicefiscale.R;

/**
 * Created by nicola on 08/01/17.
 */

public class InfoFragment extends Fragment {

    private TextView infoTextView;
    private TextView contactUsTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.info_fragment, container, false);

        infoTextView = ((TextView) view.findViewById(R.id.info_text_view));
        contactUsTextView = ((TextView) view.findViewById(R.id.contact_us_text_view));

        // Allow links to be clickable
        MovementMethod movementMethod = LinkMovementMethod.getInstance();
        infoTextView.setMovementMethod(movementMethod);
        contactUsTextView.setMovementMethod(movementMethod);

        return view;
    }
}
