package com.gmail.ndev.codicefiscale.model;

import android.support.v4.app.INotificationSideChannel;
import android.text.TextUtils;
import android.transition.ChangeBounds;

import com.gmail.ndev.codicefiscale.BuildConfig;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by nicola on 20/12/16.
 */

public class CFGenerator implements ICFGenerator {
    @Override
    public String generateName(String name) {
        String asciiName = name.replaceAll("[^a-zA-Z]", "");
        char[] letters = asciiName.toCharArray();

        List<Character> consonants = new LinkedList<>();
        List<Character> vowels = new LinkedList<>();

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < letters.length; i++) {
            char c = letters[i];
            if ("aeiou".indexOf(c) == -1) {
                consonants.add(c);
                if (consonants.size() == 4) {
                    break;
                }
            } else {
                vowels.add(c);
            }
        }

        if (consonants.size() == 4) {
            consonants.remove(1);
        }

        while (result.length() < 3 && consonants.size() > 0) {
            result.append(consonants.remove(0));
        }

        while (result.length() < 3 && vowels.size() > 0) {
            result.append(vowels.remove(0));
        }

        while (result.length() < 3) {
            result.append('X');
        }

        return result.toString().toUpperCase();
    }

    @Override
    public String generateSurname(String surname) {
        String asciiSurname = surname.replaceAll("[^a-zA-Z]", "");
        char[] letters = asciiSurname.toCharArray();

        StringBuilder result = new StringBuilder();
        // Look for consonants
        for (int i = 0; i < letters.length && result.length() < 3; i++) {
            char c = letters[i];
            if ("aeiou".indexOf(c) == -1) {
                result.append(c);
            }
        }

        // Look for vowels
        if (result.length() < 3) {
            for (int i = 0; i < letters.length && result.length() < 3; i++) {
                char c = letters[i];
                if ("aeiou".indexOf(c) > -1) {
                    result.append(c);
                }
            }
        }

        while (result.length() < 3) {
            result.append('X');
        }
        return result.toString().toUpperCase();
    }

    @Override
    public String generateDateOfBirth(LocalDate dateOfBirth, Gender gender) {
        int year = dateOfBirth.getYearOfCentury();
        int month = dateOfBirth.getMonthOfYear();
        int day = dateOfBirth.getDayOfMonth();

        if (gender == Gender.FEMALE) {
            day += 40;
        }

        StringBuilder builder = new StringBuilder();
        builder.append(year);
        builder.append(ICFGenerator.MONTH_MAPPINGS[month-1]); // Months are zero-indexed
        if (day < 10) {
            builder.append('0');
        }
        builder.append(day);

        if (BuildConfig.DEBUG && builder.length() != 5) {
            throw new AssertionError("Date of birth length must be 5");
        }
        return builder.toString().toUpperCase();
    }

    @Override
    public Character computeControlCode(String cf) {
        if (!isValidWithoutControlCode(cf))
            return null;

        // If the provided fiscal code already has the control code, then strip it away
        if (cf.length() == 16) {
            cf = cf.substring(0, 15);
        }

        char[] letters = cf.toUpperCase().toCharArray();

        int total = 0;
        for (int i = 0; i < letters.length; i++) {
            total += CharacterMappingUtil.translateCharacter(letters[i], (i + 1) % 2 == 0);
        }

        return (char) ('A' + (total % 26));

    }

    @Override
    public String[] computeOmocodes(String cf) {
        if (!isValid(cf))
            return null;

        // We need to work on the original fiscal code
        String originalFiscalCode;
        if (isOmocode(cf)) {
            originalFiscalCode = computeOriginalFiscalCode(cf);
        } else {
            originalFiscalCode = cf;
        }

        // It will contain the resulting 127 variants of the provided fiscal code
        List<String> result = new ArrayList<>(127);
        // The starting point for the computation is the original fiscal code
        result.add(originalFiscalCode);

        /*
         * Only digits are replaced in this computation.
         * Digits are at position 6, 7, 9, 10, 12, 13, 14.
         */
        int[] positions = {6, 7, 9, 10, 12, 13, 14};

        // We can replace from 1 to 7 digits at the same time
        for (int i = positions.length - 1; i >= 0; i--) {
            // It will contain all the variants computed during this iteration
            List<String> elementsToBeInserted = new LinkedList<>();

            /*
             * This is an incremental procedure.
             * During each iteration, we take all the variants computed until now and replace only
             * the character in position "position[i]", to create a new variant.
             */
            for (int j = 0; j < result.size(); j++) {
                // The new variant will be only 15 characters long, because the control code will be computed later
                char[] newVariant = new char[15];

                // Copy the first 15 characters of the j-th variant
                System.arraycopy(result.get(j).toCharArray(), 0, newVariant, 0, 15);

                // Replace the digit at position "position[i]"
                newVariant[positions[i]] = CharacterMappingUtil.OMOCODE_REPLACEMENT[Character.getNumericValue(newVariant[positions[i]])];

                // Let's create the new variant
                StringBuilder builder = new StringBuilder();
                builder.append(newVariant);
                // Compute the control code
                char controlCode = computeControlCode(builder.toString());
                // Append it to the new variant
                builder.append(controlCode);

                // This new variant will be appended to the result list when this inner loop ends
                elementsToBeInserted.add(builder.toString());
            }

            // Append all the computed variants to the results list
            result.addAll(elementsToBeInserted);
        }

        // Remove the provided fiscal code from the results list, in order to show only its variants
        result.remove(cf);

        return result.toArray(new String[result.size()]);
    }

    @Override
    public boolean isOmocode(String cf) {
        Pattern pattern = Pattern.compile("[a-z]{6}\\d{2}[abcdehlmprst]\\d{2}[a-z]\\d{3}[a-z]", Pattern.CASE_INSENSITIVE);

        Matcher matcher = pattern.matcher(cf);

        return !matcher.matches();
    }

    private boolean isValidWithoutControlCode(String cf) {
        if (TextUtils.isEmpty(cf))
            return false;

        Pattern pattern = Pattern.compile("^" + // start
                        "[a-z]{6}" + // name + surname
                        "[\\dlmnpqrstuv]{2}" + // year of birth + omocode letters
                        "[abcdehlmprst]" + // month of birth
                        "[\\dlmnpqrstuv]{2}" + // day of birth + omocode letters
                        "[a-z][\\dlmnpqrstuv]{3}" + // place of birth + omocode letters
                        "[a-z]?" + // optional control code
                        "$", // end
                Pattern.CASE_INSENSITIVE);

        Matcher matcher = pattern.matcher(cf);
        if (!matcher.matches())
            return false;

        /*
         * The day of birth must be in range 1-31 or 41-71.
         */

        String dayString = cf.substring(9, 11);

        StringBuilder tmp = new StringBuilder();
        // If the day is composed by letters, it means that it is a variant. We need the original one
        for (Character letter : dayString.toCharArray()) {
            if (!Character.isDigit(letter)) {
                tmp.append(CharacterMappingUtil.getDigitForOmocodeReplacement(letter));
            } else {
                tmp.append(Character.getNumericValue(letter));
            }
        }

        int day = Integer.parseInt(tmp.toString());

        return (day >= 1 && day <= 31) || (day >= 41 && day <= 71);

    }

    @Override
    public boolean isValid(String cf) {
        if (isValidWithoutControlCode(cf) && cf.length() == 16) {
            // Check if the control code is correct
            char controlCode = computeControlCode(cf);
            return cf.charAt(15) == controlCode;
        }
        return false;
    }

    @Override
    public String computeOriginalFiscalCode(String cf) {
        if (!isValid(cf)) {
            return null;
        }

        if (!isOmocode(cf)) {
            return cf;
        }

        char[] characters = cf.toCharArray();

        /*
         * A fiscal code is an omocode fiscal code if any number is replaced with a letter,
         * so we need to check only characters at position 6, 7, 9, 10, 12, 13, 14.
         */
        int[] positions = {6, 7, 9, 10, 12, 13, 14};
        for (int position : positions) {
            if (!Character.isDigit(characters[position])) {
                int digit = CharacterMappingUtil.getDigitForOmocodeReplacement(characters[position]);
                characters[position] = ((char) ('0' + digit));
            }
        }

        String result = new String(characters);
        // We changed some letters, so we need to compute the control code again
        result = result.substring(0, result.length() - 1);
        char controlCode = computeControlCode(result);

        return result + controlCode;
    }
}
