package com.gmail.ndev.codicefiscale.model;

import org.joda.time.LocalDate;

/**
 * Created by nicola on 08/01/17.
 */

public interface ICFReverser {

    /**
     * Returns the date of birth from the provided fiscal code, or <code>null</code> if the fiscal code is invalid.
     *
     * @param fiscalCode
     * @return
     */
    LocalDate extractDateOfBirth(String fiscalCode);

    /**
     * Extracts the code associated to the place of birth, or <code>null</code> if either the code or the fiscal code is invalid.
     *
     * @param fiscalCode
     * @return
     */
    String extractPlaceOfBirthCode(String fiscalCode);

    /**
     * Returns the gender from the provided fiscal code, or <code>null</code> if the fiscal code is invalid.
     *
     * @param fiscalCode
     * @return
     */
    Gender extractGender(String fiscalCode);
}
