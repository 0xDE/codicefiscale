package com.gmail.ndev.codicefiscale.model;

import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.support.v7.widget.RecyclerView;

import static android.view.View.NO_ID;

/**
 * Created by nicola on 01/01/17.
 */

public abstract class CursorAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    private final Callback mCallback;
    private final Context mContext;
    private Cursor mCursor;
    private int mIdColumnIndex;

    CursorAdapter(Context context, Cursor cursor, Callback callback) {
        this.mCursor = cursor;
        this.mContext = context;
        this.mCallback = callback;
        init();
    }

    public Context getContext() {
        return mContext;
    }

    private void init() {
        setHasStableIds(true);
        mIdColumnIndex = mCursor.getColumnIndexOrThrow(BaseColumns._ID);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        if (mCursor == null) {
            throw new IllegalStateException("Cursor not set");
        }

        if (!mCursor.moveToPosition(position)) {
            throw new IllegalStateException("Cannot move to position: " + position);
        }

        onBindViewHolder(holder, mCursor);
    }

    abstract void onBindViewHolder(VH holder, Cursor cursor);

    @Override
    public long getItemId(int position) {
        if (mCursor == null)
            return NO_ID;
        if (!mCursor.moveToPosition(position))
            throw new IllegalStateException("Cannot move cursor to position: " + position);
        return mCursor.getLong(mIdColumnIndex);
    }

    @Override
    public int getItemCount() {
        if (mCursor == null) {
            return 0;
        }

        return mCursor.getCount();
    }

    /**
     * The same as swapCursor, but also closes the old cursor
     *
     * @param newCursor
     */
    public void changeCursor(Cursor newCursor) {
        Cursor old = swapCursor(newCursor);

        if (old != null)
            old.close();
    }

    /**
     * Swap the cursor
     *
     * @param newCursor
     * @return
     */
    public Cursor swapCursor(Cursor newCursor) {
        if (newCursor == mCursor)
            return mCursor;

        final Cursor oldCursor = mCursor;
        mCursor = newCursor;

        if (mCursor != null) {
            mIdColumnIndex = mCursor.getColumnIndexOrThrow(BaseColumns._ID);
        } else {
            mIdColumnIndex = -1;
        }

        notifyDataSetChanged();
        return oldCursor;
    }

    public void removeItem(long id, int position) {
        mCallback.deleteItem(id);
        changeCursor(mCallback.requery());
    }

    public interface Callback {
        Cursor requery();

        void deleteItem(long id);
    }
}
