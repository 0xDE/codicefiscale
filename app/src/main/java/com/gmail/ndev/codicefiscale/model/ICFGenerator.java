package com.gmail.ndev.codicefiscale.model;

import org.joda.time.LocalDate;

public interface ICFGenerator {

    char[] MONTH_MAPPINGS = {'A', 'B', 'C', 'D', 'E', 'H', 'L', 'M', 'P', 'R', 'S', 'T'};

    /**
     * Computes the 3 letters for the name.
     * These 3 letters are either:
     * - The first three consonants of the first name, if the first name has exactly three consonants;
     * - The first, third and fourth consonant of the first name, if the first name has more than 3 consonants.
     * - The first one or two consonants followed by respectively the first two or one vowels, if the first name has less than three consonants.
     * If the resulting string length is less than 3, then the code is completed by adding as many "X" as needed to reach a length of three.
     *
     * @param name The first name(s) of the user.
     * @return 3 uppercase letters generated from the name according to the aforementioned rules.
     */
    String generateName(String name);

    /**
     * Computes the 3 letters for the surname.
     * These 3 letters are either:
     * - The first three consonants of the surname(s), if the surname(s) has at least three consonants;
     * - The first one or two consonants of the surname(s) followed by respectively the first two or one vowels of the surname(s), if the surname has less than 3 consonants;
     * If the resulting string length is less than 3, then the code is completed by adding as many "X" as needed to reach a length of three.
     *
     * @param surname The surname(s) of the user.
     * @return 3 uppercase letters generated from the surname(s) according to the aforementioned rules.
     */
    String generateSurname(String surname);

    /**
     * Computes the 5 characters related to the user's date of birth.
     * The first 2 digits are the last 2 digits of the year of birth.
     * The third letter represents the month of birth.
     * The last 3 digits are the day of birth of the user. If the user is a woman, this number is increased by 40.
     *
     * @param dateOfBirth The user's date of birth.
     * @return The 5 characters representing the user's date of birth.
     */
    String generateDateOfBirth(LocalDate dateOfBirth, Gender gender);

    /**
     * Computes the control code from the first 15 characters
     *
     * @param cf The first 15 characters of the fiscal code.
     * @return The control code, or <code>null</code> if the provided fiscal code is not valid.
     */
    Character computeControlCode(String cf);

    /**
     * Computes all the 127 variants of a given fiscal code.
     *
     * @param cf The fiscal code used to compute the variants
     * @return An array containing all the 127 variants of the given fiscal code,
     * or <code>null</code> if the provided fiscal code is not valid.
     */
    String[] computeOmocodes(String cf);

    /**
     * Checks whether a given fiscal code is an omocode or not. If the provided code is invalid, this method returns <code>false</code>.
     *
     * @param cf The fiscal code to check.
     * @return <code>true</code> if the given fiscal code is valid and is omocode, <code>false</code> otherwise.
     */
    boolean isOmocode(String cf);

    /**
     * Checks if a given fiscal code is valid.
     *
     * @param cf The fiscal code to check
     * @return <code>true</code> if the fiscal code is valid, <code>false</code> otherwise.
     */
    boolean isValid(String cf);

    /**
     * Returns the original (i.e., non omocode) version of the provided fiscal code, or the provided fiscal code itself, if it is original.
     * If the provided fiscal code is invalid, this method returns <code>null</code>.
     *
     * @param cf A valid fiscal code.
     * @return The original fiscal code, or <code>null</code> if the provided fiscal code is not valid
     */
    String computeOriginalFiscalCode(String cf);
}
