package com.gmail.ndev.codicefiscale.model;

import org.joda.time.LocalDate;

import java.util.Calendar;

/**
 * Created by nicola on 08/01/17.
 */

public class CFReverser implements ICFReverser {

    private final ICFGenerator icfGenerator;

    public CFReverser() {
        icfGenerator = new CFGenerator();
    }

    @Override
    public LocalDate extractDateOfBirth(String fiscalCode) {
        if (!icfGenerator.isValid(fiscalCode))
            return null;

        // We need the original fiscal code to compute the date of birth
        String workingCopy = icfGenerator.computeOriginalFiscalCode(fiscalCode);
        String date = workingCopy.substring(6, 11);

        int day = Integer.parseInt(date.substring(3, 5));
        // If the fiscal code is of a woman, we need to subtract 40
        if (day > 40)
            day -= 40;


        int month;
        for (month = 0; month<ICFGenerator.MONTH_MAPPINGS.length; month++) {
            if (ICFGenerator.MONTH_MAPPINGS[month] == date.charAt(2)) {
                break;
            }
        }

        int year = Integer.parseInt(date.substring(0, 2));

        /*
         * The fiscal code does not contain any information about the century.
         * This means that the fiscal code of a person born in 1916 gives the same information
         * as the fiscal code of a person born in 2016.
         *
         * Hence, we assume that if the year is between 0 and the current year, then the date of birth
         * is in the 21th century, otherwise it is in the 20th century.
         */
        int currentYear = Calendar.getInstance().get(Calendar.YEAR) % 100;
        if (year >= 0 && year <= currentYear) {
            year += 2000;
        } else {
            year += 1900;
        }

        // In Joda, months are in range 1, ..., 12
        LocalDate result = new LocalDate(year, ++month, day);

        return result;
    }

    @Override
    public String extractPlaceOfBirthCode(String fiscalCode) {
        if (!icfGenerator.isValid(fiscalCode))
            return null;

        String original = icfGenerator.computeOriginalFiscalCode(fiscalCode);

        return original.substring(11, 15);
    }

    @Override
    public Gender extractGender(String fiscalCode) {
        if (!icfGenerator.isValid(fiscalCode))
            return null;

        String original = icfGenerator.computeOriginalFiscalCode(fiscalCode);

        int day = Integer.parseInt(original.substring(9, 11));

        return day <= 31 ? Gender.MALE : Gender.FEMALE;
    }
}
