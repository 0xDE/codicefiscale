package com.gmail.ndev.codicefiscale.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by nicola on 02/01/17.
 */

public class SimpleDividerItemDecoration extends RecyclerView.ItemDecoration {

    private Drawable mDivider;

    // Use the divider provided by Android
    private final int[] ATTRS = new int[] {android.R.attr.listDivider};

    public SimpleDividerItemDecoration(Context context) {
        // Retrieve the divider drawable
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        a.recycle();
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        drawVertical(c, parent);
    }

    /**
     * Draw the separator for a vertical list (e.g., a horizontal separator)
     * @param c The canvas on which the separator should be drawn.
     * @param parent The RecyclerView.
     */
    private void drawVertical(Canvas c, RecyclerView parent) {
        /*
         * The divider will be as wide as the row, excluding any padding.
         */
        final int left = parent.getPaddingLeft();
        final int right = parent.getWidth() - parent.getPaddingRight();

        final int childCount = parent.getChildCount();
        // We apply the divider to each element in the list
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();

            // Obtain the Y coordinates (begin/end) for each row
            final int top = child.getBottom() + params.bottomMargin;
            final int bottom = top + mDivider.getIntrinsicHeight();
            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }
}
