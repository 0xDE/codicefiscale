package com.gmail.ndev.codicefiscale.model;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by nicola on 21/12/16.
 */

public class PlaceOfBirthProvider extends ContentProvider {

    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
    public static final String AUTHORITY = "com.gmail.ndev.codicefiscale.provider";

    private static final int PLACES = 1;
    private static final int PLACES_ID = 2;
    private static final int PLACES_CODE = 3;

    static {
        // Add URIs to the UriMatcher object
        URI_MATCHER.addURI(PlaceOfBirthProvider.AUTHORITY, "places", PLACES);
        URI_MATCHER.addURI(PlaceOfBirthProvider.AUTHORITY, "places/#", PLACES_ID);
        URI_MATCHER.addURI(PlaceOfBirthProvider.AUTHORITY, "places/*", PLACES_CODE);
    }

    private PlacesOfBirthDbHelper mOpenHelper;
    private SQLiteDatabase mReadableDatabase;

    @Override
    public boolean onCreate() {
        mOpenHelper = new PlacesOfBirthDbHelper(getContext());
        mReadableDatabase = new PlacesOfBirthDbHelper(getContext()).getReadableDatabase();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        switch (URI_MATCHER.match(uri)) {
            case PLACES:
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = PlaceOfBirthEntry._ID + " ASC";
                }
                break;
            case PLACES_ID:
                selection = "_ID = ?";
                selectionArgs = new String[] { uri.getLastPathSegment() };
                break;
            case PLACES_CODE:
                selection = PlaceOfBirthEntry.COLUMN_NAME_CITY_CODE+ " = ?";
                selectionArgs = new String[] { uri.getLastPathSegment() };
                break;
            default:
                return null;
        }

        Cursor cursor = mReadableDatabase.query(PlaceOfBirthEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case PLACES:
                return "vnd.android.cursor.dir/vnd.com.gmail.ndev.codicefiscale.places";
            case PLACES_ID:
                return "vnd.android.cursor.item/vnd.com.gmail.ndev.codicefiscale.places";
            case PLACES_CODE:
                return "vnd.android.cursor.item/vnd.com.gmail.ndev.codicefiscale.places";
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
