package com.gmail.ndev.codicefiscale.model;

import android.util.Log;

import java.util.Calendar;
import java.util.Observable;

/**
 * Created by nicola on 22/12/16.
 */

public class ObservableCalendar extends Observable {

    private Calendar mCalendar;

    public ObservableCalendar(Calendar calendar) {
        this.mCalendar = calendar;
        setChanged();
    }

    public void setCalendar(Calendar calendar) {
        this.mCalendar = calendar;
        setChanged();
        notifyObservers();
    }

    public Calendar getCalendar() {
        return mCalendar;
    }
}
