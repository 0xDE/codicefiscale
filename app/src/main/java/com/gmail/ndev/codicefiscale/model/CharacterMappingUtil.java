package com.gmail.ndev.codicefiscale.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nicola on 03/01/17.
 */

public final class CharacterMappingUtil {

    private static final Map<Character, Integer> ODD_CHARACTERS_MAPPING = new HashMap<>(36);
    private static final Map<Character, Integer> EVEN_CHARACTERS_MAPPING = new HashMap<>(36);

    public static final char[] OMOCODE_REPLACEMENT = {'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V'};

    static {
        ODD_CHARACTERS_MAPPING.put('0', 1);
        ODD_CHARACTERS_MAPPING.put('9', 21);
        ODD_CHARACTERS_MAPPING.put('I', 19);
        ODD_CHARACTERS_MAPPING.put('R', 8);
        ODD_CHARACTERS_MAPPING.put('1', 0);
        ODD_CHARACTERS_MAPPING.put('A', 1);
        ODD_CHARACTERS_MAPPING.put('J', 21);
        ODD_CHARACTERS_MAPPING.put('S', 12);
        ODD_CHARACTERS_MAPPING.put('2', 5);
        ODD_CHARACTERS_MAPPING.put('B', 0);
        ODD_CHARACTERS_MAPPING.put('K', 2);
        ODD_CHARACTERS_MAPPING.put('T', 14);
        ODD_CHARACTERS_MAPPING.put('3', 7);
        ODD_CHARACTERS_MAPPING.put('C', 5);
        ODD_CHARACTERS_MAPPING.put('L', 4);
        ODD_CHARACTERS_MAPPING.put('U', 16);
        ODD_CHARACTERS_MAPPING.put('4', 9);
        ODD_CHARACTERS_MAPPING.put('D', 7);
        ODD_CHARACTERS_MAPPING.put('M', 18);
        ODD_CHARACTERS_MAPPING.put('V', 10);
        ODD_CHARACTERS_MAPPING.put('5', 13);
        ODD_CHARACTERS_MAPPING.put('E', 9);
        ODD_CHARACTERS_MAPPING.put('N', 20);
        ODD_CHARACTERS_MAPPING.put('W', 22);
        ODD_CHARACTERS_MAPPING.put('6', 15);
        ODD_CHARACTERS_MAPPING.put('F', 13);
        ODD_CHARACTERS_MAPPING.put('O', 11);
        ODD_CHARACTERS_MAPPING.put('X', 25);
        ODD_CHARACTERS_MAPPING.put('7', 17);
        ODD_CHARACTERS_MAPPING.put('G', 15);
        ODD_CHARACTERS_MAPPING.put('P', 3);
        ODD_CHARACTERS_MAPPING.put('Y', 24);
        ODD_CHARACTERS_MAPPING.put('8', 19);
        ODD_CHARACTERS_MAPPING.put('H', 17);
        ODD_CHARACTERS_MAPPING.put('Q', 6);
        ODD_CHARACTERS_MAPPING.put('Z', 23);

        EVEN_CHARACTERS_MAPPING.put('0', 0);
        EVEN_CHARACTERS_MAPPING.put('9', 9);
        EVEN_CHARACTERS_MAPPING.put('I', 8);
        EVEN_CHARACTERS_MAPPING.put('R', 17);
        EVEN_CHARACTERS_MAPPING.put('1', 1);
        EVEN_CHARACTERS_MAPPING.put('A', 0);
        EVEN_CHARACTERS_MAPPING.put('J', 9);
        EVEN_CHARACTERS_MAPPING.put('S', 18);
        EVEN_CHARACTERS_MAPPING.put('2', 2);
        EVEN_CHARACTERS_MAPPING.put('B', 1);
        EVEN_CHARACTERS_MAPPING.put('K', 10);
        EVEN_CHARACTERS_MAPPING.put('T', 19);
        EVEN_CHARACTERS_MAPPING.put('3', 3);
        EVEN_CHARACTERS_MAPPING.put('C', 2);
        EVEN_CHARACTERS_MAPPING.put('L', 11);
        EVEN_CHARACTERS_MAPPING.put('U', 20);
        EVEN_CHARACTERS_MAPPING.put('4', 4);
        EVEN_CHARACTERS_MAPPING.put('D', 3);
        EVEN_CHARACTERS_MAPPING.put('M', 12);
        EVEN_CHARACTERS_MAPPING.put('V', 21);
        EVEN_CHARACTERS_MAPPING.put('5', 5);
        EVEN_CHARACTERS_MAPPING.put('E', 4);
        EVEN_CHARACTERS_MAPPING.put('N', 13);
        EVEN_CHARACTERS_MAPPING.put('W', 22);
        EVEN_CHARACTERS_MAPPING.put('6', 6);
        EVEN_CHARACTERS_MAPPING.put('F', 5);
        EVEN_CHARACTERS_MAPPING.put('O', 14);
        EVEN_CHARACTERS_MAPPING.put('X', 23);
        EVEN_CHARACTERS_MAPPING.put('7', 7);
        EVEN_CHARACTERS_MAPPING.put('G', 6);
        EVEN_CHARACTERS_MAPPING.put('P', 15);
        EVEN_CHARACTERS_MAPPING.put('Y', 24);
        EVEN_CHARACTERS_MAPPING.put('8', 8);
        EVEN_CHARACTERS_MAPPING.put('H', 7);
        EVEN_CHARACTERS_MAPPING.put('Q', 16);
        EVEN_CHARACTERS_MAPPING.put('Z', 25);
    }

    public static int translateCharacter(char c, boolean even) {
        // Get uppercase character
        Character cObject = Character.toUpperCase(c);

        Map<Character, Integer> mappings = even ? EVEN_CHARACTERS_MAPPING : ODD_CHARACTERS_MAPPING;

        if (!mappings.containsKey(cObject)) {
            throw new IllegalStateException("Character cannot be mapped to anything");
        }

        return mappings.get(cObject);
    }

    public static int getDigitForOmocodeReplacement(char replacement) {
        for (int i = 0; i < OMOCODE_REPLACEMENT.length; i++) {
            if (OMOCODE_REPLACEMENT[i] == replacement)
                return i;
        }

        throw new IllegalArgumentException("The provided replacement ('"+replacement+"') is not valid");
    }
}
