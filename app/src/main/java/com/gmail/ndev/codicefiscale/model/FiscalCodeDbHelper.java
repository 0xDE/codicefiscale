package com.gmail.ndev.codicefiscale.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by nicola on 22/12/16.
 */

public class FiscalCodeDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "FiscalCode.db";

    private static final String SQL_CREATE_DB = "CREATE TABLE " + FiscalCodeEntry.TABLE_NAME + " (" +
            FiscalCodeEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            FiscalCodeEntry.COLUMN_CODE + " TEXT NOT NULL, " +
            FiscalCodeEntry.COLUMN_DATE_OF_BIRTH + " INTEGER NOT NULL, " + // Unix time
            FiscalCodeEntry.COLUMN_PLACE_OF_BIRTH + " TEXT NOT NULL," +
            FiscalCodeEntry.COLUMN_GENDER + " INTEGER NOT NULL," +
            FiscalCodeEntry.COLUMN_NAME + " TEXT NOT NULL, " +
            FiscalCodeEntry.COLUMN_SURNAME + " TEXT NOT NULL)";

    private static final String SQL_DELETE_DB = "DROP TABLE IF EXISTS " + FiscalCodeEntry.TABLE_NAME;

    private final Context context;

    public FiscalCodeDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_DB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_DB);
        onCreate(sqLiteDatabase);
    }
}
