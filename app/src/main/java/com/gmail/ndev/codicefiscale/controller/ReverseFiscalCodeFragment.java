package com.gmail.ndev.codicefiscale.controller;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.gmail.ndev.codicefiscale.R;
import com.gmail.ndev.codicefiscale.model.CFGenerator;
import com.gmail.ndev.codicefiscale.model.CFReverser;
import com.gmail.ndev.codicefiscale.model.Gender;
import com.gmail.ndev.codicefiscale.model.ICFGenerator;
import com.gmail.ndev.codicefiscale.model.ICFReverser;
import com.gmail.ndev.codicefiscale.model.PlaceOfBirthEntry;
import com.gmail.ndev.codicefiscale.model.PlaceOfBirthProvider;

import org.joda.time.LocalDate;

/**
 * Created by nicola on 08/01/17.
 */
public class ReverseFiscalCodeFragment extends Fragment implements View.OnClickListener {

    private TextInputEditText mFiscalCode;
    private TextView mReversedPlaceOfBirth, mReversedDateOfBirth, mIsOmocode, mGender;
    private Button mCompute;
    private ICFGenerator icfGenerator;
    private ICFReverser reverser;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        icfGenerator = new CFGenerator();
        reverser = new CFReverser();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.reverse_fragment, container, false);

        mFiscalCode = ((TextInputEditText) view.findViewById(R.id.fiscal_code));
        mReversedDateOfBirth = ((TextView) view.findViewById(R.id.reversed_date_of_birth));
        mReversedPlaceOfBirth = ((TextView) view.findViewById(R.id.reversed_place_of_birth));
        mIsOmocode = ((TextView) view.findViewById(R.id.is_omocode));
        mGender = ((TextView) view.findViewById(R.id.gender));
        mCompute = ((Button) view.findViewById(R.id.reverse_btn));

        mCompute.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.reverse_btn:

                String fiscalCode = mFiscalCode.getText().toString();
                if (!icfGenerator.isValid(fiscalCode)) {
                    mFiscalCode.setError(getString(R.string.invalid_fiscal_code));
                    break;
                }

                String isOmocodeText;
                if (icfGenerator.isOmocode(fiscalCode)) {
                    isOmocodeText = getString(R.string.omocode, icfGenerator.computeOriginalFiscalCode(fiscalCode));
                } else {
                    isOmocodeText = getString(R.string.not_omocode);
                }
                mIsOmocode.setText(isOmocodeText);

                LocalDate dateOfBirth = reverser.extractDateOfBirth(fiscalCode);
                Gender gender = reverser.extractGender(fiscalCode);
                String placeOfBirthCode = reverser.extractPlaceOfBirthCode(fiscalCode);

                Uri uri = new Uri.Builder()
                        .scheme("content")
                        .authority(PlaceOfBirthProvider.AUTHORITY)
                        .appendPath("places")
                        .appendPath(placeOfBirthCode).build();
                Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);

                String placeOfBirth;
                if (cursor.moveToFirst()) {
                    placeOfBirth = cursor.getString(cursor.getColumnIndex(PlaceOfBirthEntry.COLUMN_NAME_CITY_NAME));
                } else {
                    placeOfBirth = getString(R.string.place_of_birth_not_found);
                }

                cursor.close();

                mReversedDateOfBirth.setText(DateFormat.getDateFormat(getContext()).format(dateOfBirth.toDate()));
                mReversedPlaceOfBirth.setText(placeOfBirth);

                mGender.setText(gender == Gender.MALE ? R.string.male : R.string.female);
                break;
        }
    }
}
