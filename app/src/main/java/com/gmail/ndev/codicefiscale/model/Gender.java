package com.gmail.ndev.codicefiscale.model;

/**
 * Created by nicola on 21/12/16.
 */

public enum Gender {
    MALE,
    FEMALE
}
