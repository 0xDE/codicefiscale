package com.gmail.ndev.codicefiscale.controller;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.gmail.ndev.codicefiscale.R;
import com.gmail.ndev.codicefiscale.model.CFGenerator;
import com.gmail.ndev.codicefiscale.model.FiscalCodeDbHelper;
import com.gmail.ndev.codicefiscale.model.FiscalCodeEntry;
import com.gmail.ndev.codicefiscale.model.Gender;
import com.gmail.ndev.codicefiscale.model.ICFGenerator;
import com.gmail.ndev.codicefiscale.model.ObservableCalendar;
import com.gmail.ndev.codicefiscale.model.PlaceOfBirthEntry;
import com.gmail.ndev.codicefiscale.model.PlaceOfBirthProvider;

import org.joda.time.LocalDate;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by nicola on 22/12/16.
 */

public class InsertFiscalCodeFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, DatePickerDialog.OnDateSetListener, View.OnClickListener {

    private static final int LOADER_PLACE_OF_BIRTH = 0;

    private AutoCompleteTextView mPlaceOfBirth;
    private EditText mName, mSurname;
    private TextView mDateOfBirthTextView;

    private ObservableCalendar mSelectedDateOfBirth;
    private ICFGenerator cfGenerator;
    private SimpleCursorAdapter mAdapter;
    private long mSelectedPlaceOfBirthId = -1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cfGenerator = new CFGenerator();
        mSelectedDateOfBirth = new ObservableCalendar(Calendar.getInstance());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.insert_fiscal_code, container, false);
        mPlaceOfBirth = (AutoCompleteTextView) view.findViewById(R.id.placeOfBirtAutocomplete);
        mName = (EditText) view.findViewById(R.id.name);
        mSurname = (EditText) view.findViewById(R.id.surname);
        mDateOfBirthTextView = (TextView) view.findViewById(R.id.selectedDateOfBirth);
        view.findViewById(R.id.showCalendar).setOnClickListener(this);
        view.findViewById(R.id.submit_fiscal_code).setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAdapter = new SimpleCursorAdapter(getActivity(),
                android.R.layout.simple_list_item_2,
                null,
                new String[]{PlaceOfBirthEntry.COLUMN_NAME_CITY_NAME, PlaceOfBirthEntry.COLUMN_NAME_CITY_CODE},
                new int[]{android.R.id.text1, android.R.id.text2},
                0);

        mAdapter.setCursorToStringConverter(new SimpleCursorAdapter.CursorToStringConverter() {
            @Override
            public CharSequence convertToString(Cursor cursor) {
                return cursor.getString(cursor.getColumnIndex(PlaceOfBirthEntry.COLUMN_NAME_CITY_NAME));
            }
        });

        getLoaderManager().initLoader(LOADER_PLACE_OF_BIRTH, null, this);

        mPlaceOfBirth.setAdapter(mAdapter);
        mPlaceOfBirth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= 3)
                    getLoaderManager().restartLoader(LOADER_PLACE_OF_BIRTH, null, InsertFiscalCodeFragment.this);
            }
        });
        mPlaceOfBirth.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mSelectedPlaceOfBirthId = l;
            }
        });

        // Automatically update the TextView when a new date is chosen
        mSelectedDateOfBirth.addObserver(new Observer() {
            @Override
            public void update(Observable observable, Object o) {
                ObservableCalendar calendar = (ObservableCalendar) observable;
                mDateOfBirthTextView.setText(android.text.format.DateFormat.getMediumDateFormat(getContext()).format(calendar.getCalendar().getTime()));
            }
        });
        // Force first update of the TextField
        mSelectedDateOfBirth.notifyObservers();

    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        mSelectedPlaceOfBirthId = -1;
        return new CursorLoader(getActivity(), Uri.parse("content://" + PlaceOfBirthProvider.AUTHORITY + "/places"), new String[]{PlaceOfBirthEntry._ID, PlaceOfBirthEntry.COLUMN_NAME_CITY_NAME, PlaceOfBirthEntry.COLUMN_NAME_CITY_CODE}, PlaceOfBirthEntry.COLUMN_NAME_CITY_NAME + " LIKE ?", new String[]{"%" + mPlaceOfBirth.getText().toString() + "%"}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor o) {
        mAdapter.swapCursor(o);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    public void changeDate() {
        Calendar selectedDateOfBirth = mSelectedDateOfBirth.getCalendar();
        DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                this,
                selectedDateOfBirth.get(Calendar.YEAR),
                selectedDateOfBirth.get(Calendar.MONTH),
                selectedDateOfBirth.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMaxDate(new Date().getTime());
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        mSelectedDateOfBirth.setCalendar(new GregorianCalendar(year, month, dayOfMonth));
    }

    private boolean checkFileds() {
        boolean errors = false;
        String nonEmpty = getString(R.string.non_empty);
        if (mName.length() == 0) {
            mName.setError(nonEmpty);
            errors = true;
        }

        if (mSurname.length() == 0) {
            mSurname.setError(nonEmpty);
            errors = true;
        }

        if (mSelectedDateOfBirth.getCalendar() == null || mSelectedDateOfBirth.getCalendar().after(Calendar.getInstance())) {
            mDateOfBirthTextView.setError("You cannot be born in the future");
            errors = true;
        }

        if (mSelectedPlaceOfBirthId < 0) {
            mPlaceOfBirth.setError(getString(R.string.must_pick_one));
            errors = true;
        }

        return !errors;
    }

    private void submitFiscalCode() {
        if (checkFileds()) {
            // Retrieve place of birth code
            Cursor cursor = getActivity().getContentResolver().query(Uri.parse("content://" + PlaceOfBirthProvider.AUTHORITY + "/places/" + mSelectedPlaceOfBirthId), null, null, null, null);
            if (cursor.moveToNext()) {
                String cityCode = cursor.getString(cursor.getColumnIndex(PlaceOfBirthEntry.COLUMN_NAME_CITY_CODE));

                boolean male = ((RadioButton) getView().findViewById(R.id.radioMale)).isChecked();

                StringBuilder fiscalCode = new StringBuilder();
                fiscalCode.append(cfGenerator.generateSurname(mSurname.getText().toString()));
                fiscalCode.append(cfGenerator.generateName(mName.getText().toString()));
                fiscalCode.append(cfGenerator.generateDateOfBirth(LocalDate.fromCalendarFields(mSelectedDateOfBirth.getCalendar()), male ? Gender.MALE : Gender.FEMALE));
                fiscalCode.append(cityCode);
                fiscalCode.append(cfGenerator.computeControlCode(fiscalCode.toString()));

                SQLiteDatabase database = new FiscalCodeDbHelper(getActivity()).getWritableDatabase();

                ContentValues values = new ContentValues(6);
                values.put(FiscalCodeEntry.COLUMN_NAME, mName.getText().toString());
                values.put(FiscalCodeEntry.COLUMN_SURNAME, mSurname.getText().toString());
                values.put(FiscalCodeEntry.COLUMN_CODE, fiscalCode.toString());
                values.put(FiscalCodeEntry.COLUMN_PLACE_OF_BIRTH, mSelectedPlaceOfBirthId);
                values.put(FiscalCodeEntry.COLUMN_GENDER, male);
                // Dates are stored as Unix time (in seconds)
                values.put(FiscalCodeEntry.COLUMN_DATE_OF_BIRTH, mSelectedDateOfBirth.getCalendar().getTimeInMillis() / 1000L);

                long id = database.insert(FiscalCodeEntry.TABLE_NAME, null, values);
                database.close();

                if (id > -1) {
                    // Remove the visible fragment
                    getFragmentManager().popBackStack();
                } else
                    Snackbar.make(getView().findViewById(R.id.coordinatorLayout), R.string.error_cannot_insert_fiscal_code, Snackbar.LENGTH_LONG).show();
            } else {
                Log.e("TAG", "ERROR");
            }

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.showCalendar:
                changeDate();
                break;

            case R.id.submit_fiscal_code:
                submitFiscalCode();
                break;
        }
    }
}
