package com.gmail.ndev.codicefiscale.model;

import android.provider.BaseColumns;

/**
 * Created by nicola on 21/12/16.
 */

public interface PlaceOfBirthEntry extends BaseColumns {

    String TABLE_NAME = "places_of_birth";

    String COLUMN_NAME_CITY_NAME = "city_name";
    String COLUMN_NAME_CITY_PROVINCE = "city_province";
    String COLUMN_NAME_CITY_CODE = "city_code";
}
