package com.gmail.ndev.codicefiscale.model;

import android.provider.BaseColumns;

/**
 * Created by nicola on 22/12/16.
 */

public interface FiscalCodeEntry extends BaseColumns {
    String TABLE_NAME = "fiscal_code";

    String COLUMN_CODE = "code";
    String COLUMN_NAME = "name";
    String COLUMN_SURNAME = "surname";
    String COLUMN_DATE_OF_BIRTH = "date_of_birth";
    String COLUMN_PLACE_OF_BIRTH = "place_of_birth";
    String COLUMN_GENDER = "gender";
}
