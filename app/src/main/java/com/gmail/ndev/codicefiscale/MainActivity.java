package com.gmail.ndev.codicefiscale;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.gmail.ndev.codicefiscale.controller.InfoFragment;
import com.gmail.ndev.codicefiscale.controller.ListFiscalCodeFragment;
import com.gmail.ndev.codicefiscale.controller.ReverseFiscalCodeFragment;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private BottomNavigationView mBottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBottomNavigationView = ((BottomNavigationView) findViewById(R.id.bottom_navigation));
        mBottomNavigationView.setOnNavigationItemSelectedListener(this);

        // Show first fragment
        mBottomNavigationView.getMenu().performIdentifierAction(R.id.menu_show, 0);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment nextFragment;

        switch (item.getItemId()) {
            case R.id.menu_show:
                nextFragment = new ListFiscalCodeFragment();
                break;
            case R.id.menu_reverse:
                nextFragment = new ReverseFiscalCodeFragment();
                break;
            case R.id.menu_info:
                nextFragment = new InfoFragment();
                break;
            default:
                return false;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.container, nextFragment).commit();
        return true;
    }
}
