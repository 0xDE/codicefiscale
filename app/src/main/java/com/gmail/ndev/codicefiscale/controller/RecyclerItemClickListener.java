package com.gmail.ndev.codicefiscale.controller;

import android.content.ContentValues;
import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by nicola on 03/01/17.
 */

public class RecyclerItemClickListener extends RecyclerView.SimpleOnItemTouchListener {

    private final RecyclerView mRecyclerView;

    public interface OnItemClickListener {
        /**
         * Called when the user clicks an View from RecyclerView
         * @param view
         * @param position
         */
        void onItemClick(View view, int position, long id);
    }

    private final GestureDetectorCompat mGestureDetector;
    private final Context mContext;
    private OnItemClickListener mListener;

    public RecyclerItemClickListener(Context context, RecyclerView recyclerView) {
        super();
        mContext = context;
        mRecyclerView = recyclerView;
        mGestureDetector = new GestureDetectorCompat(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                View child = mRecyclerView.findChildViewUnder(e.getX(), e.getY());

                if (child != null && mListener != null) {
                    mListener.onItemClick(child, mRecyclerView.getChildAdapterPosition(child), mRecyclerView.getChildItemId(child));
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return mGestureDetector.onTouchEvent(e);
    }

    public void setListener(OnItemClickListener mListener) {
        this.mListener = mListener;
    }
}
